//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { css } from './components/css';

// create a component
class MyClass extends Component {
    render() {
        return (
            <View style={css.container}>
                <View style={css.body}>
                    <Text style={css.textStyle}>MyClass</Text>
                    <TextInput style={css.textInputStyle} placeholder='Masukkan Nama'/>
                    <TouchableOpacity style={css.buttonStyle}>
                        <View style={css.buttonViewStyle}>
                            <Text style={css.textButtonStyle}>Klik Disini</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    } 
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default MyClass;
