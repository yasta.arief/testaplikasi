//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
class Blink extends Component {
    constructor() {
        super();
        this.state = {
            isShowingText: true,
            text: ''
        };

        setInterval(() => (
            this.setState(previousState => (
                { isShowingText: !previousState.isShowingText }
            ))
        ), 1000);
    }

    componentDidMount(){
        this.setState({
            text:'Arief Advani'
        })
    }

    render() {
        if (!this.state.isShowingText) {
            return null;
        }

        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
                <Text>{this.state.text}</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Blink;
