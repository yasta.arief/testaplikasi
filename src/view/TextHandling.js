//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert } from 'react-native';

// create a component
class TextHandling extends Component {
    constructor(){
        super();
        this.state={
            textUsername:''
        }
    }
    clickdisini(){
        Alert.alert(this.state.textUsername);
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    style={{borderBottomWidth:1, borderBottomColor:'#000'}}
                    placeholder={'Masukkan Username'}
                    onChangeText = {(text)=>this.setState({textUsername:text})}
                />
                <TouchableOpacity onPress={()=>this.clickdisini()}>
                    <Text>Klik Disini</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default TextHandling;
