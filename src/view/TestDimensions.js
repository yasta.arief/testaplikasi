//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
// create a component
class TestDimensions extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{backgroundColor:'#93bcff', height:100, width:300}}></View>
                <View style={{marginTop:30, backgroundColor:'#93bcff', height: height/8, width:width/1}}></View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:90,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default TestDimensions;
