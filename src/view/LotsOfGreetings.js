//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Greeting from './Greeting';

// create a component
class LotsOfGreetings extends Component {
    clickAlert(){
        
    }
    render() {
        let pic = {
            uri:'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
        };

        return (
            <View style={{ alignItems: 'center', marginTop:90 }}>
                {/* <Greeting name='Arief Advani' />
                <Greeting name='Jaina' />
                <Greeting name='Valeera' /> */}
                <Image source={pic} style={{width:140, height:90}} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default LotsOfGreetings;
