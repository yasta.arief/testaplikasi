import {StyleSheet, Dimensions} from 'react-native';
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export const css = StyleSheet.create({
    container:{
        flex:1
    },
    body:{
        marginTop:width/2,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#fff'
    },
    textStyle:{
        fontSize:20,
        fontWeight:'bold'
    },
    textInputStyle:{
        marginTop:60,
        width:140,
        height:60,
        borderBottomWidth:1
    },
    buttonStyle:{
        marginTop:20,
    },
    buttonViewStyle:{
        height:50,
        width:130,
        backgroundColor:'#8251ff',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
    },
    textButtonStyle:{
        fontSize:20,
        fontWeight:'bold',
        color:'#fff'
    }
});