/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/view/TextHandling';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
